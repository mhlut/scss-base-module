# SCSS base

This module is a base primer for SCSS codebases, which works in conjunction
with [Normalize.css](https://necolas.github.io/normalize.css/).

## How to use

Store the module's code in a directory called "base" (or any other name of your
choosing).

Import the base module in any another module. The import has been split into
two files, so the second half the base module can use variables declared in the
first half.

It is possible to override default values before or inbetween the `@import`
rules.

### Example

```scss
@import "../base/import_1";

// Breakpoints
$screen-size-medium:  490 * $px_em;
$screen-size-large:   960 * $px_em;
$screen-size-xlarge:  1440 * $px_em;
$screen-size-xxlarge: 1920 * $px_em;

@import "../base/import_2";
```

## Author

Originally developed by [Marijke Luttekes](mailto:info@marijkeluttekes.nl).

## License

MIT licensed. See included `LICENSE` file for more information.
